output "vpc_ID" {
  value       = module.vpc-module.vpc_id
  description = "The name of the Auto Scaling Group"
}

